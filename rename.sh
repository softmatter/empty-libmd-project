#!/bin/bash
if [ $# -eq 0 ]
then
    echo "rename.sh: usage is ./rename.sh: [project name]"
else 
    project="$1"
    old=`ls *.cc | awk -F . '{print $1}'`
    perl -pi -e s,$old,$project,g Makefile
    mv $old.cc $project.cc
    rm -rf .git
    rm -rf libmd
    rm -f .gitsubmodules
    git init
    git add Makefile
    git add $project.cc
    git submodule add git@bitbucket.org:/zuiden/libmd.git libmd
    git submodule init
    git submodule update
    cd libmd
    git checkout devel
    cd ..
    git commit -a -m "First commit"'!'
    echo "rename.sh: done, and placed first commit."
    echo "rename.sh: don't forget to create a remote git repository"'!'
    echo "rename.sh: happy coding and good luck"'!'
    echo "rename.sh: ...goodbye"'!'
    rm -f README
    rm -f rename.sh
fi
exit 0

