LIBMDDIR=libmd
include libmd/Makeheader

elp: elp.cc libmd/libmd.cc libmd/libmd.h libmd/libmd-src/* libmd/libmd-src/md/*
	$(CC) $(CCFLAGS) -o elp elp.cc 

clean: 
	rm elp
